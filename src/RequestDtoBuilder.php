<?php

declare(strict_types=1);

namespace Bittacora\Support\Dtos;

use Bittacora\Support\Dtos\Exceptions\InvalidDtoClassException;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @template T
 */
class RequestDtoBuilder
{
    /**
     * @param FormRequest $request
     * @param T $dtoClass
     * @phpstan-param class-string<T> $dtoClass
     */
    public function __construct(private readonly FormRequest $request, private readonly string $dtoClass)
    {
    }

    /**
     * @throws InvalidDtoClassException
     * @return T
     */
    public function toDto()
    {
        $dto = $this->dtoClass::map($this->request->validated());

        if (!is_a($dto, Dto::class)) {
            throw new InvalidDtoClassException();
        }

        return $dto;
    }
}
