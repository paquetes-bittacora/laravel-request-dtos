<?php

declare(strict_types=1);

namespace Bittacora\Support\Dtos\Exceptions;

class InvalidDtoClassException extends \Exception
{
    protected $message = 'La clase especificada no implementa la interfaz Dto';
}
