<?php

declare(strict_types=1);

namespace Bittacora\Support\Dtos;

interface Dto
{
    /**
     * @param array<string, string|array<string>> $data
     */
    public static function map(array $data): self;
}
